import java.sql.*;
import java.util.Scanner;

public class Task1 {
    private static final String url = "jdbc:mysql://localhost:3306/test123";
    private static final String user = "root", pass = "root";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Intrare in program...");

        System.out.print("Doriti sa inregistrati un student nou? da/nu: ");
        String daNu = sc.next();

        raspuns1(daNu);

        System.out.print("Introduce numele studentului: ");
        String lastNameI = sc.next();

        System.out.print("Introduce prenume student: ");
        String firstNameI = sc.next();

        System.out.print("Introduce grupa student: ");
        String nameGroupI = sc.next();

        System.out.print("Introduce IDNP student: ");
        String idnpI = sc.next();

        System.out.print("Introduce nota test1: ");
        int note1I = sc.nextInt();

        System.out.print("Introduce nota test2: ");
        int note2I = sc.nextInt();

        System.out.print("Introduce nota examen: ");
        int examI = sc.nextInt();

        System.out.print("Doriti sa aflati media aritmetica? da/nu: ");
        String answer1 = sc.next();

        raspuns2(answer1);

        double arithmeticMeanI = (((note1I + note2I) / 2.0) + examI) / 2.0;
        System.out.print("Media aritmetica finala: " + arithmeticMeanI);

        System.out.println();
        System.out.print("Doriti sa aflati informatie despre student? da/nu: ");
        String answer2 = sc.next();

        raspuns3(answer2);

        System.out.println("Numele: " + lastNameI);
        System.out.println("Prenumele: " + firstNameI);
        System.out.println("Grupa: " + nameGroupI);
        System.out.println("IDNP: " + idnpI);
        System.out.println("Nota1: " + note1I);
        System.out.println("Nota2: " + note2I);
        System.out.println("Nota examen: " + examI);
        System.out.println("Media aritmetica: " + arithmeticMeanI);
        System.out.println("Iesire din program...");

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection connect = DriverManager.getConnection(url, user, pass);

            Statement statement = connect.createStatement();

            statement.executeUpdate("insert into exammariana(lastName, firstName, groupName, idnp, note1, note2, noteExam, arithmeticMean) " +
                    "values('" + lastNameI + "','" + firstNameI + "','" + nameGroupI + "'," + idnpI + "," + note1I + "," + note2I + ", " + examI + "," + arithmeticMeanI + ")");

            ResultSet resultSet = statement.executeQuery("select * from exammariana");

            while (resultSet.next()) {
                String lastName = resultSet.getString(1);
                String fisrtName = resultSet.getString(2);
                String groupName = resultSet.getString(3);
                String idnp = resultSet.getString(4);
                int note1 = resultSet.getInt(5);
                int note2 = resultSet.getInt(6);
                int noteExam = resultSet.getInt(7);
                double arithmeticMean = resultSet.getDouble(8);
                System.out.printf("%s %s %s %s %s %s %s %s%n", lastName, fisrtName, groupName, idnp, note1, note2, noteExam, arithmeticMean);
            }
        } catch (SQLException |
                 ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void raspuns1(String daNu) {
        if (daNu.equals("da")) {

        } else if (daNu.equals("nu")) {
            System.exit(0);
        }
    }

    public static void raspuns2(String answer1) {
        if (answer1.equals("da")) {

        } else if (answer1.equals("nu")) {
            System.exit(0);
        }
    }

    public static void raspuns3(String answer2) {
        if (answer2.equals("da")) {

        } else if (answer2.equals("nu")) {
            System.exit(0);
        }
    }
}
